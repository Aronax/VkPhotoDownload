﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Windows.Forms;
using VkNet;
using VkNet.Enums.Filters;
using VkNet.Exception;
using VkNet.Model.Attachments;
using VkNet.Model.RequestParams;

namespace VkPhotoSave
{
    public partial class Form1 : Form
    {
        private readonly VkApi _api = new VkApi();

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Login(loginTextBox.Text, passwordTextBox.Text);
        }

        public void Login(string login, string password)
        {
            var od = new FolderBrowserDialog();

            if (od.ShowDialog() == DialogResult.OK)
            {
                var dowloadFolder = od.SelectedPath;

                try
                {
                    _api.Authorize(new ApiAuthParams
                    {
                        ApplicationId = 5061635,
                        Login = login,
                        Password = password,
                        Settings = Settings.All
                    });
                }
                catch (VkApiAuthorizationException ex)
                {
                    MessageBox.Show("wrong login or password");
                    return;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return;
                }

                var albums = _api.Photo.GetAlbums(new PhotoGetAlbumsParams());
                var pageSize = 200;

                var totalPhotos = new List<Photo>();

                var photosInfo = _api.Photo.GetAll(new PhotoGetAllParams
                {
                    Extended = true,
                    Count = 1
                });

                var pages = (int) photosInfo.TotalCount / pageSize + 1;

                Log($"Total count {photosInfo.TotalCount}");
                for (var i = 1; i <= pages; i++)
                {
                    Log(
                        $"get photos info {totalPhotos.Count} - {totalPhotos.Count + pageSize} of {photosInfo.TotalCount}");
                    var photos = _api.Photo.GetAll(new PhotoGetAllParams
                    {
                        Extended = true,
                        Count = (ulong) pageSize,
                        Offset = (ulong) totalPhotos.Count
                    });
                    totalPhotos.AddRange(photos);

                    Thread.Sleep(350);
                }

                Log("Downlad photos");

                var n = 0;
                foreach (var photo in totalPhotos)
                {
                    n++;
                    var uri = GetPhotoUrl(photo);
                    var client = new WebClient();

                    var fileName = Path.GetFileName(uri.AbsolutePath);

                    var downloadPath = Path.Combine(dowloadFolder, fileName);

                    if (photo.AlbumId.HasValue)
                    {
                        var album = albums.FirstOrDefault(x => x.Id == photo.AlbumId);
                        if (album != null)
                        {
                            var folder = Path.Combine(dowloadFolder, album.Title.Replace("<", "")
                                .Replace(">", "")
                                .Replace(":", "")
                                .Replace("\"", "")
                                .Replace("//", "")
                                .Replace("|", "")
                                .Replace("?", "")
                                .Replace("*", ""));

                            downloadPath = Path.Combine(folder, fileName);
                            if (!Directory.Exists(folder))
                                Directory.CreateDirectory(folder);
                        }
                    }

                    Log($"{n} of {totalPhotos.Count}, {downloadPath}");

                    if (!File.Exists(downloadPath))
                    {
                        client.DownloadFile(uri, downloadPath);

                        if (File.Exists(downloadPath) && photo.CreateTime.HasValue)
                        {
                            File.SetCreationTime(downloadPath, photo.CreateTime.Value);
                            File.SetLastWriteTime(downloadPath, photo.CreateTime.Value);
                            File.SetLastAccessTime(downloadPath, photo.CreateTime.Value);
                        }
                    }
                }
            }
        }


        private Uri GetPhotoUrl(Photo photo)
        {
            if (photo.Photo2560 != null)
                return photo.Photo2560;

            if (photo.Photo1280 != null)
                return photo.Photo1280;

            if (photo.Photo807 != null)
                return photo.Photo807;

            if (photo.Photo604 != null)
                return photo.Photo604;

            if (photo.Photo130 != null)
                return photo.Photo130;

            if (photo.Photo75 != null)
                return photo.Photo75;


            return null;
        }

        private void Log(string text)
        {
            listBox1.Items.Add(text);
            listBox1.SelectedIndex = listBox1.Items.Count - 1;
            Application.DoEvents();
        }
    }
}